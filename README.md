ISS Localisator
========================

Simple application. Styleless. Display current ISS location. Not meant for deployment.

## 1) Requirements

PHP 7.1

## 2) Install dependencies with composer

composer install

## 3) Tests

php vendor/bin/phpunit

## 4) Running on PHP Build In Server

php -S localhost:8000

## 5) Open in bworser

http://localhost:8000