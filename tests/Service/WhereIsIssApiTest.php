<?php

namespace App\Tests\Service;

use PHPUnit\Framework\TestCase;
use App\Model\IssLocation;
use App\Exception\ApiException;
use App\Service\WhereIsIssApi;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Client;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
class WhereIsIssApiTest extends TestCase
{
    /* @var $httpClient Client */
    protected $httpClient;
    /* @var $subject WhereIsIssApi */
    protected $subject;

    protected function setUp()
    {
        $this->httpClient = $this->getMockBuilder(Client::class)->setMethods(['get'])->getMock();
        $this->subject    = new WhereIsIssApi($this->httpClient);
    }

    public function testGetCurrentLocationSuccess()
    {
        $latitude  = 1;
        $longitude = 2;
        $data      = [
            'latitude'  => $latitude,
            'longitude' => $longitude
        ];

        $response = $this->getMockBuilder(Response::class)->setMethods(['getBody', 'getStatusCode'])->getMock();
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn(json_encode($data));
        $response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);

        $apiUrl = sprintf('https://api.wheretheiss.at/v1/satellites/%s', WhereIsIssApi::ISS_SATELLITE_ID);
        $this->httpClient->expects($this->once())
            ->method('get')
            ->with($apiUrl)
            ->willReturn($response);

        $result = $this->subject->getCurrentLocation();

        $this->assertEquals(new IssLocation($latitude, $longitude), $result);
    }

    public function testGetCurrentLocationException()
    {
        $response = $this->getMockBuilder(Response::class)->setMethods(['getStatusCode'])->getMock();
        $response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(500);

        $apiUrl = sprintf('https://api.wheretheiss.at/v1/satellites/%s', WhereIsIssApi::ISS_SATELLITE_ID);
        $this->httpClient->expects($this->once())
            ->method('get')
            ->with($apiUrl)
            ->willReturn($response);

        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('Unexpected results from WhereIsIss API.');

        $this->subject->getCurrentLocation();
    }
}