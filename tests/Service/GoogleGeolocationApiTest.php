<?php

namespace App\Tests\Service;

use PHPUnit\Framework\TestCase;
use App\Service\GoogleGeolocationApi;
use GuzzleHttp\Client;
use App\Model\IssLocation;
use GuzzleHttp\Psr7\Response;
use App\Exception\ApiException;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
class GoogleGeolocationApiTest extends TestCase
{
    /* @var $httpClient Client */
    protected $httpClient;
    /* @var $subject GoogleGeolocationApi */
    protected $subject;

    protected function setUp()
    {
        $this->httpClient = $this->getMockBuilder(Client::class)->setMethods(['get'])->getMock();
        $this->subject    = new GoogleGeolocationApi($this->httpClient);
    }

    /**
     * @dataProvider getStatusResults
     */
    public function testGetHumanReadableAddressSuccess($status, $expectedResult)
    {
        $this->addResponseAssertions($status);
        $result = $this->subject->getHumanReadableAddress(new IssLocation(1, 2));
        $this->assertEquals($expectedResult, $result);
    }

    public function getStatusResults()
    {
        return [
            [GoogleGeolocationApi::API_STATUS_OK, 'ISS is currently flying over: foo'],
            [GoogleGeolocationApi::API_STATUS_ZERO_RESULTS, 'We cannot determine ISS exact address. It is probably over the seas.'],
        ];
    }

    public function testGetHumanReadableAddressException()
    {
        $status = 'error';
        $this->addResponseAssertions($status);

        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('Unexpected results from Google Geolocation API. ' . $status);

        $this->subject->getHumanReadableAddress(new IssLocation(1, 2));
    }

    /**
     * @param string $status
     */
    protected function addResponseAssertions($status)
    {
        $data = [
            'results' => [
                ['formatted_address' => 'foo']
            ],
            'status' => $status
        ];
        $response = $this->getMockBuilder(Response::class)->setMethods(['getBody'])->getMock();
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn(json_encode($data));

        $httpClientOptions = ['query' => [
            'latlng' => '1,2',
            'key'    => GoogleGeolocationApi::GOOGLE_APP_KEY
        ]];
        $this->httpClient->expects($this->once())
            ->method('get')
            ->with(GoogleGeolocationApi::API_URL, $httpClientOptions)
            ->willReturn($response);
    }
}