<?php

namespace App\Tests\Controller;

use PHPUnit\Framework\TestCase;
use App\Controller\IssLocationController;
use App\Service\IssLocalisatorInterface;
use App\Service\ReverseGeolocationInterface;
use App\Model\IssLocation;
use App\Exception\ApiException;
use App\View\ViewInterface;
use App\View\TwigView;

/**
 * @author mkrawczyk
 */
class IssLocationControllerTest extends TestCase
{
    /* @var $localisator IssLocalisatorInterface */
    protected $localisator;
    /* @var $reverseGeolocationEngine ReverseGeolocationInterface */
    protected $reverseGeolocationEngine;
    /* @var $view ViewInterface */
    protected $view;

    protected function setUp()
    {
        $this->localisator              = $this->getMockBuilder(IssLocalisatorInterface::class)->setMethods(['getCurrentLocation'])->getMock();
        $this->reverseGeolocationEngine = $this->getMockBuilder(ReverseGeolocationInterface::class)->setMethods(['getHumanReadableAddress'])->getMock();
        $this->view                     = new TwigView();

        $this->subject = new IssLocationController($this->localisator, $this->reverseGeolocationEngine, $this->view);
    }

    public function testShowLocationSuccess()
    {
        $issLocation = new IssLocation(1, 2);
        $address = 'address';
        $addressData = [
            'address' => $address,
            'location' => $issLocation
        ];
        $templateString = $this->view->render('ShowLocation', $addressData);

        $this->localisator->expects($this->once())
            ->method('getCurrentLocation')
            ->willReturn($issLocation);
        $this->reverseGeolocationEngine->expects($this->once())
            ->method('getHumanReadableAddress')
            ->with($issLocation)
            ->willReturn($address);

        $result = $this->subject->showLocation();
        $this->assertEquals($templateString, $result);
    }
}