<?php

require __DIR__ . '/vendor/autoload.php';

$app = \App\App::getInstance();
$app->run();