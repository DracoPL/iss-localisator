<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Service\IssLocalisatorInterface;
use App\Service\ReverseGeolocationInterface;
use App\Model\IssLocation;
use App\Exception\ApiException;
use App\View\ViewInterface;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
class IssLocationController
{
    /* @var $localisator IssLocalisatorInterface */
    protected $localisator;
    /* @var $reverseGeolocationEngine ReverseGeolocationInterface */
    protected $reverseGeolocationEngine;
    /* @var $view ViewInterface */
    protected $view;

    /**
     * @param IssLocalisatorInterface $localisator
     * @param ReverseGeolocationInterface $reverseGeolocationEngine
     * @param ViewInterface $view
     */
    public function __construct(IssLocalisatorInterface $localisator, ReverseGeolocationInterface $reverseGeolocationEngine, ViewInterface $view)
    {
        $this->localisator              = $localisator;
        $this->reverseGeolocationEngine = $reverseGeolocationEngine;
        $this->view                     = $view;
    }

    /**
     * @return string
     */
    public function showLocation() : string
    {
        try {
            /* @var $issLocation IssLocation */
            $issLocation = $this->localisator->getCurrentLocation();
            $issAddress = $this->reverseGeolocationEngine->getHumanReadableAddress($issLocation);

            return $this->view->render('ShowLocation',
                [
                    'address' => $issAddress,
                    'location' => $issLocation
                ]
            );
        } catch (ApiException $ex) {

            return $this->view->render('Error',
                [
                    'info' => 'Sorry, we cannot determine ISS location. Try again later.',
                    'error' => $ex->getMessage()
                ]
            );
        }
    }
}