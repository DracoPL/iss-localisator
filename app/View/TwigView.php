<?php

namespace App\View;

use App\View\ViewInterface;
use Twig_Loader_Filesystem;
use Twig_Environment;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
class TwigView implements ViewInterface
{
    protected $twig;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(__DIR__ . '/Templates');
        $twig   = new Twig_Environment($loader, ['auto_reload' => true]);

        $this->twig = $twig;
    }

    public function render(string $template, array $data) : string
    {
        return $this->twig->render($template . '.html.twig', $data);
    }
}