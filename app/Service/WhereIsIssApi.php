<?php

namespace App\Service;

use App\Model\IssLocation;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use App\Exception\ApiException;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
class WhereIsIssApi implements IssLocalisatorInterface
{
    const ISS_SATELLITE_ID = 25544;

    /* @var $httpClient ClientInterface */
    protected $httpClient;

    /**
     * @param ClientInterface $httpClient
     */
    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return IssLocation
     * @throws ApiException
     */
    public function getCurrentLocation(): IssLocation
    {
        /* @var $response ResponseInterface */
        $response = $this->httpClient->get($this->getSatellitePositionUrl());

        if ($response->getStatusCode() !== 200) {
            throw new ApiException('Unexpected results from WhereIsIss API.');
        }

        $data = json_decode((string) $response->getBody(), true);
        $issLocation = new IssLocation($data['latitude'], $data['longitude']);

        return $issLocation;
    }

    /**
     * @return string
     */
    protected function getSatellitePositionUrl()
    {
        return sprintf('https://api.wheretheiss.at/v1/satellites/%s', self::ISS_SATELLITE_ID);
    }
}