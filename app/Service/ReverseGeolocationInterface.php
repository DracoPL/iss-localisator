<?php

namespace App\Service;

use App\Model\IssLocation;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
interface ReverseGeolocationInterface
{
    /**
     * @param IssLocation $issLocation
     */
    public function getHumanReadableAddress(IssLocation $issLocation);
}