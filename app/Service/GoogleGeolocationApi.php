<?php

namespace App\Service;

use App\Model\IssLocation;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use App\Exception\ApiException;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
class GoogleGeolocationApi implements ReverseGeolocationInterface
{
    const GOOGLE_APP_KEY          = 'AIzaSyCEdn09oHdD16x4MW9PQ7P7heEMmN9n_O4';
    const API_URL                 = 'https://maps.googleapis.com/maps/api/geocode/json';
    const API_STATUS_OK           = 'OK';
    const API_STATUS_ZERO_RESULTS = 'ZERO_RESULTS';

    /* @var $httpClient ClientInterface */
    protected $httpClient;

    /**
     * @param ClientInterface $httpClient
     */
    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param IssLocation $issLocation
     * @return string
     * @throws ApiException
     */
    public function getHumanReadableAddress(IssLocation $issLocation) : string
    {
        /* @var $response ResponseInterface */
        $response = $this->httpClient->get(self::API_URL, [
            'query' => [
                'latlng' => sprintf('%s,%s', $issLocation->getLatitude(), $issLocation->getLongitude()),
                'key'    => self::GOOGLE_APP_KEY
            ]
        ]);

        $data = json_decode((string)$response->getBody(), true);

        if (!in_array($data['status'], [self::API_STATUS_OK, self::API_STATUS_ZERO_RESULTS])) {
            throw new ApiException('Unexpected results from Google Geolocation API. ' . $data['status']);
        }

        if ($data['status'] === self::API_STATUS_ZERO_RESULTS) {
            return 'We cannot determine ISS exact address. It is probably over the seas.';
        }

        return $this->getMostSpecificAddress($data['results']);
    }

    /**
     * @param array $results
     * @return string
     */
    protected function getMostSpecificAddress(array $results) : string
    {
        return 'ISS is currently flying over: ' . $results[0]['formatted_address'];
    }
}