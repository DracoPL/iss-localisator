<?php

namespace App\Service;

use App\Model\IssLocation;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
interface IssLocalisatorInterface
{
    public function getCurrentLocation() : IssLocation;
}