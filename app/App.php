<?php

namespace App;

use App\Controller\IssLocationController;
use App\Service\WhereIsIssApi;
use App\Service\GoogleGeolocationApi;
use GuzzleHttp\Client as GuzzleClient;
use App\View\TwigView;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
class App
{
    private static $instance;

    private function __construct() {}

    /**
     * @return self $instance
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof App) {
            self::$instance = new App;
        }

        return self::$instance;
    }

    /**
     * Run the application. Right now using only one Controller without any kind of routing.
     */
    public function run()
    {
        $httpClient           = new GuzzleClient();
        $whereIsIssApi        = new WhereIsIssApi($httpClient);
        $googleGeolocationApi = new GoogleGeolocationApi($httpClient);
        $view                 = new TwigView();

        $controller = new IssLocationController($whereIsIssApi, $googleGeolocationApi, $view);
        $viewString = $controller->showLocation();

        echo $viewString;
    }
}
