<?php

namespace App\Exception;

use Exception;

/**
 * Just a new class for a context exception.
 *
 * @author michal.krawczyk@vip.wp.pl
 */
class ApiException extends Exception
{

}