<?php

namespace App\Model;

/**
 * @author michal.krawczyk@vip.wp.pl
 */
class IssLocation
{
    /* @var $latitude float */
    protected $latitude;
    /* @var $longitude float */
    protected $longitude;

    /**
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct($latitude, $longitude)
    {
        $this->latitude  = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
}